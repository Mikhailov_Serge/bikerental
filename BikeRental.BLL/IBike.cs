﻿using BikeRental.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BikeRental.BLL
{
    public interface IBike : IDisposable
    {
        IEnumerable<Bike> GetBikes();
        IEnumerable<DAL.Models.Type> GetTypes();
        IEnumerable<Status> GetStatuses();
        Bike GetBikeById(Guid bikeId);
        void InsertBike(Bike bike);
        void DeleteBike(Guid bikeId);
        void UpdateBike(Bike bike);
        void Save();
        bool BikeExist(Guid bikeId);
    }
}
