﻿using BikeRental.DAL.Model;
using BikeRental.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace BikeRental.DAL
{
    public class BikeRentalContext : DbContext
    {
        public BikeRentalContext(DbContextOptions<BikeRentalContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new BikeConfiguration());
            modelBuilder.ApplyConfiguration(new StatusConfiguration());
            modelBuilder.ApplyConfiguration(new TypeConfiguration());
        }
        public DbSet<Bike> Bikes { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<Type> Types { get; set; }
    }
}