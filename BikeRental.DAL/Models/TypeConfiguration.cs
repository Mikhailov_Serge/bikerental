﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BikeRental.DAL.Models
{
    public class TypeConfiguration : IEntityTypeConfiguration<Type>
    {
        public void Configure(EntityTypeBuilder<Type> builder)
        {
            builder.HasData(
                new Type[]
                {
                    new Type {Id = 1, Name = "Road" },
                    new Type {Id = 2, Name = "Mountain" },
                    new Type {Id = 3, Name = "Common" },
                });
        }
    }
}
