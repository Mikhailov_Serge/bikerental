﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BikeRental.DAL.Models
{
    public class StatusConfiguration : IEntityTypeConfiguration<Status>
    {
        public void Configure(EntityTypeBuilder<Status> builder)
        {
            builder.HasData(
                new Status[]
                {
                    new Status { Id = 1, Name = "Available"},
                    new Status { Id = 2, Name = "Rented" }
                });
        }
    }
}
