﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BikeRental.BLL;
using BikeRental.DAL;
using BikeRental.DAL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BikeRentalAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BikeController : ControllerBase
    {
        private readonly BikeRepository _repository;

        public BikeController(BikeRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Bike>> GetBikes()
        {
            return Ok(_repository.GetBikes());
        }

        // GET api/users/5
        [HttpGet("{id}")]
        public ActionResult<Bike> GetBike(Guid id)
        {
            var bike = _repository.GetBikeById(id);

            if (bike == null)
            {
                return NotFound();
            }

            return bike;
        }

        // POST api/users
        [HttpPost]
        public ActionResult<Bike> PostBike(Bike bike)
        {
            bike.StatusId = 1; // Status available
            _repository.InsertBike(bike);
            _repository.Save();

            return CreatedAtAction("GetBike", new { id = bike.Id }, bike);
        }

        // DELETE: api/Bike/5
        [HttpDelete("{id}")]
        public ActionResult<Bike> DeleteBike(Guid id)
        {
            var bike = _repository.GetBikeById(id);
            if (bike == null)
            {
                return NotFound();
            }

            _repository.DeleteBike(id);
            _repository.Save();

            return bike;
        }

        // PUT api/users/
        [HttpPut]
        public IActionResult PutBike(Bike bike)
        {
            if (!_repository.BikeExist(bike.Id))
            {
                return NotFound();
            }
            _repository.UpdateBike(bike);

            try
            {
                _repository.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_repository.BikeExist(bike.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpGet]
        [Route("[action]")]
        public ActionResult<IEnumerable<BikeRental.DAL.Models.Type>> GetTypes()
        {
            return Ok(_repository.GetTypes());
        }

        [HttpGet]
        [Route("[action]")]
        public ActionResult<IEnumerable<Status>> GetStatuses()
        {
            return Ok(_repository.GetStatuses());
        }
    }
}